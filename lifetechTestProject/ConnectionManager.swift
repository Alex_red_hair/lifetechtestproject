import Foundation

class ConnectionManager {
    static let shared = ConnectionManager()
    
    func getProducts(completion: @escaping (_ result: Products?, _ error: Error?) -> Void) {
        guard let url = URL(string:
            "https://s3-eu-west-1.amazonaws.com/developer-application-test/cart/list")
            else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, _, error) in
            if error == nil, let data = data {
                if let jsonResponse = try? JSONDecoder().decode(Products.self, from: data) {
                    completion(jsonResponse, nil)
                } else {
                    completion(nil, NSError(domain: "problems with decoding data",
                                            code: 0,
                                            userInfo: [:]))
                }
            } else {
                completion(nil, error)
            }
        }
        task.resume()
    }
    
    func getProductDetails(productID: String, completion: @escaping (_ result: ProductResponse?, _ error: Error?) -> Void) {
        guard let url = URL(string:
            "https://s3-eu-west-1.amazonaws.com/developer-application-test/cart/\(productID)/detail")
            else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, _, error) in
            if error == nil, let data = data {
                if let jsonResponse = try? JSONDecoder().decode(ProductResponse.self, from: data) {
                    completion(jsonResponse, nil)
                } else {
                    completion(nil, NSError(domain: "problems with decoding data",
                                            code: 0,
                                            userInfo: [:]))
                }
            } else {
                completion(nil, error)
            }
        }
        task.resume()
    }
}
