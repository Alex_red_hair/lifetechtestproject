import UIKit

class ReuseProductView: UIView {

    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "ReuseProductView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    func setupView(withProduct product: Product) {
        self.productNameLabel.text = product.name
        
        self.productPriceLabel.text = "\(Int(product.price))"
        
        self.productDescriptionLabel.text = product.descrip
        
        self.productImageView.image = product.imageData?.getImage()
    }
}
