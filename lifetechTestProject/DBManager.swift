import Foundation
import RealmSwift

class DBManager {
    let realm = try! Realm()
    
    func saveProducts(_ products: [Product]) {
        try! self.realm.write {
            self.realm.add(products)
        }
    }
    
    func saveProduct(_ product: Product) {
        try! self.realm.write {
            self.realm.add(product)
        }
    }
    
    func obtainProducts() -> [Product] {
        return Array(self.realm.objects(Product.self))
    }
    
    func setDescription(_ description: String, for product: Product) {
        try! self.realm.write {
            product.descrip = description
        }
    }
    
    func delete(product: Product) {
        try! self.realm.write {
            self.realm.delete(product)
        }
    }
}

extension Data {
    func getImage() -> UIImage? {
        guard let image = UIImage(data: self) else {
            return nil
        }
        return image
    }
}
