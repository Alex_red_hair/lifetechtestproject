import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    func setupCell(withProduct product: Product) {
        self.productNameLabel.text = product.name
        self.productPriceLabel.text = "\(Int(product.price))"
        self.productImageView.image = product.imageData?.getImage()
    }
}
