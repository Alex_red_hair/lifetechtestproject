import UIKit
import RealmSwift

class ProductResponse: Codable {
    let id, name, imageURL: String
    let price: Double
    var description: String?
    var imageData: Data?
    
    enum CodingKeys: String, CodingKey {
        // Product 5 (steak) has a mistake in the  key "description", so I added key "decription"
        case id = "product_id", name, price, imageURL = "image", description, decription
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.id, forKey: .id)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.price, forKey: .price)
        try container.encode(self.imageURL, forKey: .imageURL)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.price = try container.decode(Double.self, forKey: .price)
        self.imageURL = try container.decode(String.self, forKey: .imageURL)
        self.description = try container.decodeIfPresent(String.self, forKey: .description)
        if self.description == nil {
            self.description = try container.decodeIfPresent(String.self, forKey: .decription)
        }
        self.imageData = self.setImageData()
    }
    
    init(from realmProduct: Product) {
        self.id = realmProduct.id
        self.name = realmProduct.name
        self.price = realmProduct.price
        self.imageURL = realmProduct.imageURL
        self.imageData = realmProduct.imageData
        self.description = realmProduct.description
    }
    
    func setImageData() -> Data? {
        if let url = URL(string: self.imageURL) {
            if let data = try? Data(contentsOf: url) {
                return data
            } else { return nil }
        } else {
            return nil
        }
    }
}

@objcMembers
class Product: Object {
    dynamic var id: String = ""
    dynamic var name: String = ""
    dynamic var price: Double = 0
    dynamic var imageURL: String = ""
    dynamic var imageData: Data? = nil
    dynamic var descrip: String? = nil
}

class Products: Codable {
    let products: [ProductResponse]
}
