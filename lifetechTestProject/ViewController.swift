import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var productsCollectionView: UICollectionView!
    
    let fullSizeImageView = UIImageView()
    let dbManager = DBManager()
    var products = [Product]()
    var imageXib = ReuseProductView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.obtainData()
    }

    private func obtainData() {
        self.products = self.dbManager.obtainProducts()
        if self.products.isEmpty {
            self.getProductsList()
        }
    }
    
    private func getProductsList() {
        ConnectionManager.shared.getProducts { [weak self] result, error in
            DispatchQueue.main.async {
                if let error = error {
                    self?.showToast(message: error.localizedDescription)
                } else {
                    if let productsList = result?.products {
                        if !productsList.isEmpty {
                            for product in productsList {
                                let newProduct = Product(value: ["id" : product.id,
                                                                 "name" : product.name,
                                                                 "price" : product.price,
                                                                 "imageURL" : product.imageURL,
                                                                 "imageData" : product.imageData,
                                                                 "descrip" : product.description])
                                self?.dbManager.saveProduct(newProduct)
                                self?.products.append(newProduct)
                            }
                        }
                        self?.productsCollectionView.reloadData()
                    }
                }
            }
        }
    }
    
    private func showToast(message : String) {
//        DispatchQueue.main.async {
            let toastLabel = UILabel()
            let height = message.height(withConstrainedWidth: self.view.frame.width - 100, font: toastLabel.font)
            toastLabel.frame = CGRect(x: self.view.center.x - (self.view.frame.width - 100)/2,
                                      y: self.view.frame.height - 50 - height,
                                      width: self.view.frame.width - 100,
                                      height: height + 20)
            toastLabel.backgroundColor = UIColor.gray
            toastLabel.textColor = UIColor.white
            toastLabel.textAlignment = .center;
            toastLabel.text = message
            toastLabel.lineBreakMode = .byWordWrapping
            toastLabel.numberOfLines = 0
            toastLabel.layer.cornerRadius = 20;
            toastLabel.clipsToBounds = true
            self.view.addSubview(toastLabel)
            UIView.animate(withDuration: 0.3, delay: 2, options: .curveLinear, animations: {
                toastLabel.alpha = 0.0
            }, completion: {(_) in
                toastLabel.removeFromSuperview()
            })
//        }
    }
}

// MARK: - UICollectionView methods
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as? ProductCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.setupCell(withProduct: self.products[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width-10)/2, height: (collectionView.frame.width-10)/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.showDetail(product: self.products[indexPath.row])
    }
    
    func showDetail(product: Product) {
        if product.descrip != nil {
            self.showXib(withProduct: product)
        } else {
            ConnectionManager.shared.getProductDetails(productID: product.id) { [weak self] result, error in
                DispatchQueue.main.async {
                    if let error = error {
                        self?.showXib(withProduct: product)
                        self?.showToast(message: error.localizedDescription)
                    } else {
                        if let productDescription = result?.description {
                            self?.dbManager.setDescription(productDescription, for: product)
                        }
                        self?.showXib(withProduct: product)
                    }
                }
            }
        }
    }
    
    private func showXib(withProduct product: Product) {
        guard let xib = ReuseProductView.instanceFromNib() as? ReuseProductView else { return }
        self.imageXib = xib
        self.imageXib.setupView(withProduct: product)
        self.imageXib.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        let tapRecognaizer = UITapGestureRecognizer(target: self, action: #selector(tapRecognaized(_:)))
        self.imageXib.addGestureRecognizer(tapRecognaizer)
        self.view.addSubview(self.imageXib)
        self.view.bringSubviewToFront(self.imageXib)
    }
    
    @IBAction func tapRecognaized(_ sender: UITapGestureRecognizer) {
        self.imageXib.removeFromSuperview()
    }
}

// MARK: - String extension for calculating size
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
